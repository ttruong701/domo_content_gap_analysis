// Some DataSets are massive and will bring any web browser to its knees if you
// try to load the entire thing. To keep your app performing optimally, take
// advantage of filtering, aggregations, and group by's to bring down just the
// data your app needs. Do not include all columns in your data mapping file,
// just the ones you need.
//
// For additional documentation on how you can query your data, please refer to
// https://developer.domo.com/docs/dev-studio/dev-studio-data

domo.get('/data/v1/semrush?limit=100')
  .then(function (semrush) {
    console.log("semrush", semrush);
  });

var svg
var w = 1000
var h = 500
var padding = 30
var data

svg = d3.select("body")
        .insert("svg")
        .attr("width", w)
        .attr("height", h)
        .attr("fill", "pink")
// svg = d3.select("#svg")
//         .append("svg")
//         .attr("width", w)
//         .attr("height", h)

data = domo.get('/data/v1/semrush?limit=100')
var dataset = [ 5, 10, 13, 19, 21, 25, 22, 18, 15, 13,
                11, 12, 15, 20, 18, 17, 16, 18, 23, 25 ]
svg.selectAll("rect")
  .data(dataset)
  .enter()
  .append("rect")
  .attr("x", function (d, i) {
    return i * (w / dataset.length) // Width of 20 plus 1 for padding
  })
  .attr("y", function (d) {
    return h - d * 4
  })
  .attr("width", w / dataset.length - padding)
  .attr("height", function (d) {
    return d * 4
  })
  .attr("fill", function (d) {
    return "rgb(0, 0, " + (d * 10) + ")"
  }) // encode data with color

// var text = svg.selectAll("txt")
//               .data(data)
//               .enter()
//               .append("text")
//               .text(function(d) {
//                 return d["Keyword"]
//               })

