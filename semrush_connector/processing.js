"use strict";

/**
 * @file Creates Domo DataSets from RDI SEMrush data
 * @summary Connects SEMrush API to Domo
 * @author Tommy Truong <ttruong@reddoor.biz>
 * @version 0.0.2
 */

var NOTHING_FOUND = 'ERROR 50 :: NOTHING FOUND';

var input = {
  'report': metadata.report.toLowerCase(),
  'parameters': metadata.parameters,
  'apiKey': metadata.account.apikey,
  'url':'http://api.semrush.com/', 
  'batchDate': new Date().toISOString()
}

function getURL(input) {
  var url = input['url'] 
    + '?type=' + input['report']
    + '&key=' + input['apiKey']
    + '&display_limit=' + input['parameters']['display_limit']
    + '&export_columns=Ph,Po,Pp,Pd,Nq,Ur,Cp,Tr,Tc,Co,Nr,Td' 
    + '&domain=' + input['parameters']['domain']
    + '&display_sort=tr_desc&database=us';
  
  return url;
}

function getData(url) {
  var res = httprequest.get(url).split("\n"); 
  return (res[0] !== NOTHING_FOUND) ? res : null;
}

/**
 * Sets table columns
 * @param {object} datagrid 
 * @param {string[]} column_names 
 * @returns {null}
 */
function setColumns(datagrid, columnNames) {
  columnNames.forEach(function (column) {
    datagrid.addColumn(column, datagrid.DATA_TYPE_STRING);
  });
}

/**
 * Populates a row for a metric datapoint
 * @param {object} datagrid 
 * @param {string[]} data 
 */
function fillRows(datagrid, data, batchDate) {
  data.forEach(function (datum) {
    datum.split(";").forEach(function (point) {
      datagrid.addCell(point);
    });
    datagrid.addCell(batchDate);
    datagrid.endRow();
  });
}

function buildDatasetPartial(datagrid, input, data) {
  if (!(data)) {
    DOMO.log(NOTHING_FOUND);
    return null;
  }
  var columnNames = data[0].split(';'),
      dataNoHeader = data.slice(1);
  columnNames.push(input['batchDate']);

  setColumns(datagrid, columnNames)
  fillRows(datagrid, dataNoHeader, input['batchDate'])
}

var buildDataset = buildDatasetPartial.bind(null, datagrid, input)

var url = getURL(input);
var data = getData(url)
buildDataset(data)
