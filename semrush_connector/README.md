# SEMrush Connectors
by Tommy Truong, Marketing Technology Intern, Summer '17

## Introduction
Using the SEMrush API, I will be creating a custom Domo connector
with RDI data.

## Getting Started
[SEMrush API](https://www.semrush.com/api-documentation/)