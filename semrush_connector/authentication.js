"use strict";

/**
 * @file Authenticates SEMrush Domo Connector 
 *       by checking Red Door Interactive's API units count
 * @summary Authenticates connector
 * @author Tommy Truong <ttruong@reddoor.biz>
 * @version 0.0.2
 */

var url = "http://www.semrush.com/users/countapiunits.html?key=" + metadata.account.apikey;
var res = httprequest.get(url);
parseInt(res) > 0 ? auth.authenticationSuccess() : auth.authenticationFailed();