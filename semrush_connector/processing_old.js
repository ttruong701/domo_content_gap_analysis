"use strict";

/**
 * Sets table columns
 * @param {object} datagrid 
 * @param {string[]} column_names 
 * @returns {null}
 */
function setColumns(datagrid, column_names) {
  column_names.forEach(function (column) {
    datagrid.addColumn(column, datagrid.DATA_TYPE_STRING);
  });
}
/**
 * Populates a row for a metric datapoint
 * @param {object} datagrid 
 * @param {string[]} data 
 */
function fillRows(datagrid, data, batch_date) {
  data.forEach(function (datum) {
    datum.split(";").forEach(function (point) {
      datagrid.addCell(point);
    });
    datagrid.addCell(batch_date);
    datagrid.endRow();
  });
}
/**
 * Driver function
 * @see setColumns
 * @see fillRows
 * @param {object} state 
 * @param {string} state.url
 * @param {object} state.datagrid
 * @param {string} state.batch_date
 * 
 */
function buildReport(state) {
  var res = httprequest.get(state.url).split("\n");

  if (res[0] !== "ERROR 50 :: NOTHING FOUND") {
    setColumns(state.datagrid, res[0].split(";").concat(["batch_date"]));

    fillRows(state.datagrid, res.slice(1), state.batch_date);
  } else {
    DOMO.log("Bad/misspelled domain! No results found.");
  }
}
buildReport({
  "url": "http://api.semrush.com/?" + "type=" + metadata.report.toLowerCase() + "&key=" + metadata.account.apikey + "&display_limit=" + metadata.parameters["display_limit"] + "&export_columns=Ph,Po,Pp,Pd,Nq,Ur,Cp,Tr,Tc,Co,Nr,Td" + "&domain=" + metadata.parameters["domain"] + "&display_sort=tr_desc" + "&database=us",
  "datagrid": datagrid,
  "batch_date": new Date().toISOString()
});